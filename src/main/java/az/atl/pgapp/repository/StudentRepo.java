package az.atl.pgapp.repository;

import az.atl.pgapp.config.DbConfig;
import az.atl.pgapp.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {

    public boolean save(Student student) {
        DbConfig dbConfig = new DbConfig();
        Connection connection = dbConfig.connection();
        int result = 0;

        try {
            PreparedStatement ps =
                    connection.prepareStatement("insert into student values (?,?,?,?)");
            ps.setLong(1, student.getId());
            ps.setString(2, student.getName());
            ps.setString(3, student.getSurname());
            ps.setString(4, student.getEmail());

            result = ps.executeUpdate();
            System.out.println(result);
            dbConfig.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result==0?false:true;
    }

    public Student getById(Long id) {
        return new Student();
    }

    public List<Student> list() {
        List<Student> students = new ArrayList<>();

        DbConfig dbConfig = new DbConfig();
        Connection connection = dbConfig.connection();
        try {
            PreparedStatement ps =
                    connection.prepareStatement("select * from student");


            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getLong("id"));
                student.setName(resultSet.getString("name"));
                student.setSurname(resultSet.getString("surname"));
                student.setEmail(resultSet.getString("email"));
                students.add(student);
            }

            dbConfig.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return students;
    }

}

package az.atl.pgapp.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConfig {

    private Connection conn=null;
    public Connection connection(){
        String url="jdbc:postgresql://localhost:5432/atlacademy?stringtype=unspecified";
        String  username="postgres";
        String password="root";
        try {
            conn= DriverManager.getConnection(url,username,password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return conn;

    }
    public void close(){
        if(conn !=null){
            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(new DbConfig().connection());
    }


}
